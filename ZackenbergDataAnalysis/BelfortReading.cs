﻿using System;

namespace ZackenbergDataAnalysis
{
    class BelfortReading
    {
        public BelfortReading()
        {
            Date = DateTime.Now.ToString();
        }

        public string Date { get; set; }
        public string Time { get; set; }
        public decimal Precipitation_Belfort { get; set; }
        public string Status { get; set; }
        public int _370_ID { get; set; }
    }
}
