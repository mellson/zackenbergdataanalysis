﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using LinqToExcel;
using NodaTime;

namespace ZackenbergDataAnalysis
{
    class Program
    {
        static void Main()
        {
            var excel = new ExcelQueryFactory("ZackenbergData-Belfort.xlsx");
            var belfortReadings = excel.Worksheet<BelfortReading>().ToList();
            var sensorReadings = new List<SensorReading>();

            var f = belfortReadings.Where(reading => reading.Status != "OK");

            decimal lastGoodPrecipitation = 0;
            var lastReading = new BelfortReading();
            for (var i = 0; i < belfortReadings.Count; i++)
            {
                var date = DateTime.Parse(belfortReadings[i].Date);
                var yearMonth = date.Year + " - " + date.Month;
                var hours = GetHours(belfortReadings[i].Time);
                date = date.AddHours(hours);
                var status = belfortReadings[i].Status == null ? "ok" : belfortReadings[i].Status.Trim().ToLower();

                var precipitation = belfortReadings[i].Precipitation_Belfort - lastReading.Precipitation_Belfort;

                // If we go into a new year or there is a problem with the status we need to handle it
                if ((DateTime.Parse(lastReading.Date).Year < date.Year || status != "ok") &&
                    (i + 1) < belfortReadings.Count)
                {
                    // If the value is indicated as missing use the last known good value, otherwise reset the value to 0
                    precipitation = status == "missing" ? lastGoodPrecipitation : 0;
                    lastReading = belfortReadings[i + 1];
                }
                else
                    lastReading = belfortReadings[i];

                // If we encounter a negative reading set it to 0
                precipitation = precipitation < 0 ? 0 : precipitation;
                lastGoodPrecipitation = precipitation;

                sensorReadings.Add(new SensorReading
                {
                    Date = date,
                    YearMonth = yearMonth,
                    Precipitation = precipitation
                });
            }

            var file = new FileInfo("weather.csv");
            if (file.Exists)
                file.Delete();
            var sw = new StreamWriter(file.OpenWrite());
            var months = sensorReadings.GroupBy(reading => reading.YearMonth).Select(readings => new Month {SensorReadings = readings.ToList(), StartOfMonth = readings.First().Date}).ToList();
            foreach (var month in months)
                sw.WriteLine(month.StartOfMonth.ToShortDateString() + "," + month.GetPrecipitation());
            sw.Close();
        }

        private static double GetHours(string hourString)
        {
            var value = Convert.ToDouble(hourString);
            if (value > 0) return value / 100;
            return 0;
        }
    }
}
