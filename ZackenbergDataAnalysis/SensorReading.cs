﻿using System;

namespace ZackenbergDataAnalysis
{
    class SensorReading
    {
        public DateTime Date { get; set; }
        public string YearMonth { get; set; }
        public decimal Precipitation { get; set; }
    }
}
