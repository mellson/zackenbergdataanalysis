﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZackenbergDataAnalysis
{
    class Month
    {
        public List<SensorReading> SensorReadings { get; set; }
        public DateTime StartOfMonth { get; set; }

        public string GetPrecipitation()
        {
            return SensorReadings.Sum(reading => reading.Precipitation).ToString().Replace(",",".");
        }
    }
}
